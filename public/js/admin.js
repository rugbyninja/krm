function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imagePreview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".imagePreview").change(function () {
    readURL(this);
});


$('span.newdoc').on('click', function(e){
	e.preventDefault(e);
	var newdoc = '<div class="col-md-12"><label>Document Name</label><input type="text" class="form-control jsondocs-name"><label>Make mandatory?</label><input type="checkbox" class="jsondocs-req"></div>';
	$('.eventdocs').append(newdoc);
});


$('body').on('change', '.jsondocs-name', docs);
$('body').on('change', '.jsondocs-req', docs);

function docs(){

	var values = [];
	$('.jsondocs-name').each(function(){
	if($(this).val().length > 0){

		var array = [];
		var doc = $(this).val();
		var check = $(this).parent().find('.jsondocs-req');

		if(check.is(':checked')){
			var req = 1;
		} else{
			var req = 0;
		}

		values.push({
			name: doc,
			required: req
		});
	}
});
	var string = JSON.stringify(values);
	console.log(string);
	$('input[name=documents]').val(string);
}