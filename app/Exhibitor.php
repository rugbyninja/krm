<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Exhibitor extends Authenticatable
{
    use Notifiable;
	// The authentication guard for admin
    protected $guard = 'exhibitor';
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
    protected $fillable = [
        'company', 'name', 'email', 'phone', 'address_1', 'town', 'postcode', 'notes', 'active',
    ];
     /**
      * The attributes that should be hidden for arrays.
      *
      * @var array
      */
    protected $hidden = [
        
    ];

    /**
     * Returns all exhibitors resgistered to attend a specified event
     * @param type $event 
     * @return type
     */
    public static function event(int $event)
    {
        return Exhibitor::join('attendees', 'attendees.exhibitor_id', '=', 'exhibitors.id')->where('attendees.event_id', $event)->orderBy('exhibitors.company')->get();
    }

    /**
     * Returns all information from a specified user
     * @param int $id 
     * @return type
     */
    public static function user(int $id)
    {
      return Exhibitor::where('id', $id)->first();
    }
}