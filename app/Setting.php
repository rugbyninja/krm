<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
    	'value',
    ];

    /**
     * Returns the email template for stand approvals
     * @return type
     */
    public static function approve()
    {
    	return Setting::where('ident', 'APPROVE')->first();
    }

    /**
     * Returns the email template for exhibitor deadline
     * @return type
     */
    public static function deadline()
    {
    	return Setting::where('ident', 'DEADLINE')->first();
    }

    /**
     * Returns the email template for exhibitor reminders
     * @return type
     */
    public static function reminder()
    {
    	return Setting::where('ident', 'REMINDER')->first();
    }

    /**
     * Returns the email template for stand approvals
     * @return type
     */
    public static function email()
    {
        return Setting::where('ident', 'email')->first();
    }

}
