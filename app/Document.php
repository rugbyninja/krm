<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Document extends Model
{
    protected $fillable = [
    	'event_id', 'exhibitor_id', 'type', 'document', 'status', 'notes',
    ];

    /**
     * Checks if an exhibitor has submitted a specified document against an event or not
     * @param int $ event
     * @param int $ exhibitor
     * @param string $document
     * @return type
     */
    public static function check(int $event, int $exhibitor, string $document)
    {
        $return = new \stdClass();

    	$check = Document::where([['event_id', $event],['exhibitor_id', $exhibitor],['type', $document]])->get();

    	if($check->count() > 0){
            $return->document = $check;
            $return->bool = true;
    		return $return;
    	}

        $return->bool = false;

    	return $return;
    }

    /**
     * Returns a documents status based on event id and document name
     * @param type $id 
     * @return type
     */
    public static function status(int $event, string $document)
    {
    	return Document::where([['event_id', $event],['exhibitor_id', Auth::guard('exhibitor')->user()->id],['type', $document]])->select('status')->pluck('status')->first();
    }

    /**
     * Returns a documents id based on event id and document name
     * @param type $id 
     * @return type
     */
    public static function id(int $event, string $document)
    {
        return Document::where([['event_id', $event],['exhibitor_id', Auth::guard('exhibitor')->user()->id],['type', $document]])->select('id')->pluck('id')->first();
    }

    /**
     * Returns all documents submitted by a specified exhibitor to a specified event
     * @param int $event 
     * @param int $exhibitor 
     * @return type
     */
    public static function exhibitor(int $event, int $exhibitor)
    {
        return Document::where([['event_id', $event],['exhibitor_id', $exhibitor]])->get();
    }

    /**
     * Returns all documents submitted to a specified event
     * @param int $event 
     * @param int $exhibitor 
     * @return type
     */
    public static function event(int $event, int $exhibitor)
    {
        return Document::where('event_id', $event)->get();
    }

    /**
     * Returns all documents submitted to a specified event
     * @param int $event 
     * @param int $exhibitor 
     * @return type
     */
    public static function pending(int $event)
    {
        return Document::where([['event_id', $event],['status', 'Pending']])->orderBy('created_at', 'desc')->paginate(20);
    }

    /**
     * Returns the total amount submitted out of total required for the event
     * @param type $id 
     * @return type
     */
    public static function submitted($id)
    {
        $return = new \stdClass;

        $event = \App\Event::where('id', $id)->first();
        $documents = count(json_decode($event->documents));
        $exhibitors = \App\Attendee::where('event_id', $id)->count();

        $return->submitted = Document::where('event_id', $id)->count();
        $return->total = $documents*$exhibitors;

        return $return;
    }

    /**
     * Returns the count of approved documents for a given event
     * @param type $id 
     * @return type
     */
    public static function approved($id)
    {
        return Document::where([['event_id', $id],['status', 'Approved']])->count();
    }

    /**
     * Returns the percentage completion of a given event based on approved documents
     * @param type $id 
     * @return type
     */
    public static function completion($id)
    {

        $event = \App\Event::where('id', $id)->first();
        $documents = count(json_decode($event->documents));

        if($documents==0){
            return 0;
        }

        $exhibitors = \App\Attendee::where('event_id', $id)->count();

        $approved = Document::where([['event_id', $id],['status', 'Approved']])->count();

        if($approved==0){
            return 0;
        }

        $total = $documents*$exhibitors;

        return round(($approved/$total)*100,2);
    }
}
