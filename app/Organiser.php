<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organiser extends Model
{
    protected $fillable = [
    	'organiser', 'name', 'email', 'phone', 'address_1', 'address_2', 'town', 'postcode', 'facebook', 'instagram', 'linkedin', 'twitter', 'website', 'notes', 'active',
    ];
}
