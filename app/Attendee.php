<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\EventApprove;
use App\Mail\EventDeadline;
use App\Mail\EventReminder;
use App\Event;
use App\Exhibitor;

class Attendee extends Model
{
    protected $fillable = [
    	'event_id', 'exhibitor_id', 'active',
    ];

    /**
     * Returns true if attendee has been created and false if already exists
     * @param type $event 
     * @param type $exhibitor 
     * @param type|null $stand 
     * @param type|null $dimensions 
     * @return type
     */
    public static function register($event, $exhibitor, $stand = null, $dimensions = null)
    {
    	$check = Attendee::where([['event_id', $event->id],['exhibitor_id', $exhibitor->id]])->count();

    	if($check > 0){
    		return false;
    	}

        $create = [];
        $create['event_id'] = $event->id;
        $create['exhibitor_id'] = $exhibitor->id;
        $create['standd'] = $stand;
        $create['dimensions'] = $dimensions;
        $create['active'] = 1;
        $return = Attendee::create($create);

        return true;
    }

    /**
     * Returns an object of exhibitor status against event, both a textual representation and boolean result are returned
     * @param type $event 
     * @param type $exhibitor 
     * @return type
     */
    public static function status($event, $exhibitor)
    {
        $return = new \stdClass();

        foreach(json_decode(Event::where('id', $event)->select('documents')->pluck('documents')->first()) AS $document){
            if($document->required==1){
                if(Document::where([['event_id', $event],['exhibitor_id', $exhibitor],['type', $document->name],['status', 'Approved']])->count()==0){
                    $return->text = 'Pending';
                    $return->bool = false;

                    return $return;
                }
            }
        }

        $return->text = 'Approved';
        $return->bool = true;

        return $return;
    }

    /**
     * Check a given users permission to build against a given event
     * @param type $event 
     * @param type $exhibitor 
     * @return type
     */
    public static function permission($event, $exhibitor)
    {
        $attendee = Attendee::where([['event_id', $event],['exhibitor_id', $exhibitor]])->first();

        if($attendee->permission==1){
            return true;
        }

        return false;
    }

    /**
     * Approves an exhibitors permission to build and triggers an email
     * @param type $event 
     * @param type $exhibitor 
     * @return type
     */
    public static function approve($event, $exhibitor)
    {
        try{
            $attendee = Attendee::where([['event_id', $event],['exhibitor_id', $exhibitor]])->first();
            Attendee::where('id', $attendee->id)->update(['permission'=>1]);

            $event = Event::where('id', $attendee->event_id)->first();
            $exhibitor = Exhibitor::where('id', $attendee->exhibitor_id)->first();

            Mail::to($exhibitor->email)
                  ->bcc(Setting::email()->value)
                  ->send(new EventApprove($event, $exhibitor));
        } catch(\ErrorException $e){
        }

        return redirect()->back()->with('success', 'Exhibitor successfully given permission to build');
    }

    /**
     * Sends a reminder to an exhibitor who hasn't completed all the documents
     * @param type $event 
     * @param type $exhibitor 
     * @return type
     */
    public static function reminder($event, $exhibitor)
    {
        try{
            $attendee = Attendee::where([['event_id', $event],['exhibitor_id', $exhibitor]])->first();

            $event = Event::where('id', $attendee->event_id)->first();
            $exhibitor = Exhibitor::where('id', $attendee->exhibitor_id)->first();

            Mail::to($exhibitor->email)->send(new EventReminder($event, $exhibitor));
        } catch(\ErrorException $e){
        }

        return redirect()->back()->with('success', 'Reminders successfully sent');
    }

    /**
     * Sends a deadline missed email to an exhibitor who hasn't completed all the documents
     * @param type $event 
     * @param type $exhibitor 
     * @return type
     */
    public static function deadline($event, $exhibitor)
    {
        try{
            $attendee = Attendee::where([['event_id', $event],['exhibitor_id', $exhibitor]])->first();

            $event = Event::where('id', $attendee->event_id)->first();
            $exhibitor = Exhibitor::where('id', $attendee->exhibitor_id)->first();

            Mail::to($exhibitor->email)->send(new EventDeadline($event, $exhibitor));
        } catch(\ErrorException $e){   
        }

        return redirect()->back()->with('success', 'Reminders successfully sent');
    }

    /**
     * Aproves all exhibitors with all mandatory documents approved but yet to receive their permission to build
     * @param type $event 
     * @return type
     */
    public static function approve_all($event)
    {
        $attendees = Attendee::where('event_id', $event)->get();

        foreach($attendees AS $attendee){
            if($attendee->permission==0){
                if(Attendee::status($attendee->event_id, $attendee->exhibitor_id)->bool){
                    Attendee::approve($attendee->event_id, $attendee->exhibitor_id);
                }
            }
        }

        return redirect()->back()->with('success', 'All exhibitors newly approved have now been emailed with their permission to build.');
    }

    /**
     * Aproves all exhibitors with all mandatory documents approved but yet to receive their permission to build
     * @param type $event 
     * @return type
     */
    public static function deadline_all($event)
    {
        $attendees = Attendee::where('event_id', $event)->get();

        foreach($attendees AS $attendee){
            if($attendee->permission==0){
                if(! Attendee::status($attendee->event_id, $attendee->exhibitor_id)->bool){
                    Attendee::deadline($attendee->event_id, $attendee->exhibitor_id);
                }
            }
        }

        return redirect()->back()->with('success', 'All exhibitors who have missed the deadline have been successfully emailed');
    }

    /**
     * Aproves all exhibitors with all mandatory documents approved but yet to receive their permission to build
     * @param type $event 
     * @return type
     */
    public static function reminder_all($event)
    {
        $attendees = Attendee::where('event_id', $event)->get();

        foreach($attendees AS $attendee){
            if($attendee->permission==0){
                if(! Attendee::status($attendee->event_id, $attendee->exhibitor_id)->bool){
                    Attendee::reminder($attendee->event_id, $attendee->exhibitor_id);
                }
            }
        }

        return redirect()->back()->with('success', 'All exhibitors who have not been approved have been successfully emailed with a reminder');
    }


}
