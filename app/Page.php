<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
    	'title', 'slug', 'menu', 'parent', 'auth', 'exhibitor', 'organiser', 'content', 'active', 'banner', 'caption', 'link', 'link_text',
    ];

}
