<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentResponse extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $document;
    public $event;
    public $exhibitor;

    public function __construct($document, $event, $exhibitor)
    {
        $this->document = $document;
        $this->event = $event;
        $this->exhibitor = $exhibitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.document-response')->with('document', $this->document)
                                                      ->with('event', $this->event)
                                                      ->with('exhibitor', $this->exhibitor);
    }
}
