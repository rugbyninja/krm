<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventDeadline extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $event;
    public $exhibitor;

    public function __construct($event, $exhibitor)
    {
        $this->event = $event;
        $this->exhibitor = $exhibitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.event-deadline')->with('event', $this->event)
                                                   ->with('exhibitor', $this->exhibitor);
    }
}
