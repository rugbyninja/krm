<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\DocumentResponse;
use App\Document;
use App\Event;
use App\Exhibitor;

class DocumentController extends Controller
{
    /**
     * Stores a document uploaded by the exhibitor
     * @param Request $request 
     * @return type
     */
    public function store(Request $request)
    {
    	$path = Storage::put('uploads', $request->file('document'));

    	$data = [];
    	$data['event_id'] = $request->event;
    	$data['exhibitor_id'] = Auth::guard('exhibitor')->user()->id;
    	$data['type'] = $request->type;
    	$data['document'] = $path;

        if(! empty($request->status)){
            $data['status'] = $request->status;
        }

    	Document::updateOrCreate(['id' => $request->id], $data);

    	return redirect()->back()->with('data', 'upload-success');
    }

    /**
     * Downloads a document by specified id
     * @param type $id 
     * @return type
     */
    public function download($id)
    {
        if(! Auth::check()){
            abort('404');
        }
        
        $document = Document::where('id', $id)->first();
        $exhibitor = Exhibitor::where('id', $document->exhibitor_id)->first();

        return Storage::download($document->document);
    }

    /**
     * Approves a given docment based on id
     * @param type $id 
     * @return type
     */
    public function approve(Request $request, $id)
    {
        $data = [];
        $data['status'] = 'Approved';

        if(! empty($request->message)){
            $data['notes'] = $request->message;
        }

        Document::where('id', $id)->update($data);

        $document = Document::where('id', $id)->first();
        $event = Event::where('id', $document->event_id)->first();
        $exhibitor = Exhibitor::where('id', $document->exhibitor_id)->first();

        // Mail::to($exhibitor->email)->send(new DocumentResponse($document, $event, $exhibitor));

        return redirect()->back()->with('success', 'Successfully approved document');
    }

    /**
     * Decline a given docment based on id
     * @param type $id 
     * @return type
     */
    public function decline(Request $request, $id)
    {
        $data = [];
        $data['status'] = 'Declined';

        if(! empty($request->message)){
            $data['notes'] = $request->message;
        }

        Document::where('id', $id)->update($data);

        $document = Document::where('id', $id)->first();
        $event = Event::where('id', $document->event_id)->first();
        $exhibitor = Exhibitor::where('id', $document->exhibitor_id)->first();

        Mail::to($exhibitor->email)->send(new DocumentResponse($document, $event, $exhibitor));

        return redirect()->back()->with('success', 'Successfully declined document');
    }

    /**
     * Sends a message to the exhibitor who submitted the document
     * @param type $id 
     * @return type
     */
    public function message(Request $request, $id)
    {
        $data = [];
        $data['notes'] = $request->message;

        Document::where('id', $id)->update($data);

        $document = Document::where('id', $id)->first();
        $event = Event::where('id', $document->event_id)->first();
        $exhibitor = Exhibitor::where('id', $document->exhibitor_id)->first();
        
        // Mail::to($exhibitor->email)->send(new DocumentResponse($document, $event, $exhibitor));

        return redirect()->back()->with('success', 'Successfully messaged exhibitor');
    }

    /**
     * Manually approves a document that hasn't been submitted
     * @param type $event 
     * @param type $exhibitor 
     * @param type $document 
     * @return type
     */
    public function manual($event, $exhibitor, $document)
    {
        $create = [];
        $create['event_id'] = $event;
        $create['exhibitor_id'] = $exhibitor;
        $create['type'] = $document;
        $create['document'] = 'MANUALLY APPROVED';
        $create['status'] = 'Approved';
        $create['notes'] = 'This document was manually approved without a formal submission, no download is available';

        Document::create($create);

        return redirect()->back()->with('success', 'Successfully created and approved '.$document);
    }
}
