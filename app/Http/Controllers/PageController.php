<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Exhibitor;
use App\Page;
use App\Setting;

class PageController extends Controller
{
    /**
     * @var simply sets the path for the views in the CMS - makes it simpler to copy / paste new CRUD controllers
     */
	protected $crud = 'pages';

	/**
	 * Returns a page view based on slug
	 * @param type $slug
	 * @param Request $request
	 * @return type
	 */
	public function view($slug, Request $request)
	{
		$page = [];
		$page['page'] = Page::where([['slug', $slug],['active', 1]])->first();
        if(! $page['page']){
            abort(404);
        }
        $page['navigation'] = Page::where('menu', 1)->orderBy('title', 'asc')->get();
        $page['settings'] = Setting::all()->keyBy('ident');

		return view('view')->with('page', $page)
						   ->with('request', $request);
	}

    /**
     * Returns the login form view to the user
     * @param Request $request 
     * @return type
     */
    public function login(Request $request)
    {
        if(empty($request->login)){
            $page = [];
            $page['page'] = Page::where([['slug', 'login']])->first();
            if(! $page['page']){
                abort(404);
            }
            $page['navigation'] = Page::where('menu', 1)->orderBy('title', 'asc')->get();
            $page['settings'] = Setting::all()->keyBy('ident');
            $page['protected'] = 'exhibitors.login';
            return view('view')->with('page', $page)
                               ->with('request', $request);
        } else{
            $count = Exhibitor::where('email', $request->email)->count();
            if($count==0){
                return redirect()->back()->with('error', 'email');
            } else{
                $exhibitor = Exhibitor::where('email', $request->email)->first();
                return redirect('/exhibitor/login/'.$exhibitor->id.'/'.hash('md5',$exhibitor->email));
            }
        }
    }

	/**
	 * If no slug is set this returns the home / index page
	 * @param Request $request
	 * @return type
	 */
    public function index(Request $request)
    {
    	return $this->view('home', $request);
    }

    /**
     * Returns the admin view to a CMS user
     * @return type
     */
    public function admin()
    {
    	$page = [];
        $page['crud'] = Page::orderBy('title')->get();
    	$page['view'] = 'crud.'.$this->crud.'.index';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the view to create a new crud item in the CMS
     * @return type
     */
    public function create()
    {
    	$page = [];
    	$page['view'] = 'crud.'.$this->crud.'.create';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the form to update the crud item
     * @param type $id 
     * @return type
     */
    public function update($id)
    {
    	$page = [];
    	$page['crud'] = Page::where('id', $id)->first();
    	$page['view'] = 'crud.'.$this->crud.'.update';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Updates or creates a crud item based on id
     * @param Request $request 
     * @return type
     */
    public function store(Request $request)
    {
        try{

        	$data = $request->except('_token');

            if(! empty($request->file('banner'))){
                $path = Storage::put('public/banners', $request->file('banner'));
                $data['banner'] = $path;
            }

            if(! empty($request->active)){
                $data['active'] = 1;
            } else{
                $data['active'] = 0;
            }

            if(! empty($request->menu)){
                $data['menu'] = 1;
            } else{
                $data['menu'] = 0;
            }
            
            if(empty($request->id)){
                $type = Page::create($data);
            } else{
                $type = Page::updateOrCreate(['id'=>$request->id],$data);
            }

    	    if($type->wasRecentlyCreated){
    	    	$message = 'Record successfully created';
    	    } else{
    	    	$message = 'Record successfully updated';
    	    }
        } catch(\Exception $e){
            dd($e);
            $message = 'Record could not be created, please make sure all required fields are filled out and that your slug is unique';
            return redirect()->back()->with('danger', $message);
        }

    	return redirect('/admin/'.$this->crud)->with('success', $message);
    }
}
