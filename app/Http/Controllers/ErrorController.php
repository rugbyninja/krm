<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Setting;

class ErrorController extends Controller
{
	/**
	 * Returns a page view based on slug
	 * @param type $slug
	 * @param Request $request
	 * @return type
	 */
	public function view($slug, Request $request)
	{
		$page = [];
		$page['page'] = Page::where('slug', 'error')->first();
		$page['page']->title = 'Authentication Error';
        $page['navigation'] = Page::where('menu', 1)->orderBy('title', 'asc')->get();
        $page['settings'] = Setting::all()->keyBy('ident');
        $page['error'] = $slug;

		return view('view')->with('page', $page)
						   ->with('request', $request);
	}
}
