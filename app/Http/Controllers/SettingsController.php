<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller
{
    /**
     * @var simply sets the path for the views in the CMS - makes it simpler to copy / paste new CRUD controllers
     */
	protected $crud = 'settings';

    /**
     * Returns the admin view to a CMS user
     * @return type
     */
    public function admin()
    {
    	$page = [];
        $page['crud'] = Setting::orderBy('id', 'asc')->get()->keyBy('ident');
    	$page['view'] = 'crud.'.$this->crud.'.index';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the form to update the crud item
     * @param type $id 
     * @return type
     */
    public function update($id)
    {
    	$page = [];
    	$page['crud'] = Setting::where('id', $id)->first();
    	$page['view'] = 'crud.'.$this->crud.'.update';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Updates or creates a crud item based on id
     * @param Request $request 
     * @return type
     */
    public function store(Request $request)
    {
    	$data = $request->except('_token');

        $type = Setting::updateOrCreate(['id'=>$request->id],$data);

	    $message = 'Record successfully updated';

    	return redirect('/admin/'.$this->crud)->with('success', $message);
    }
}
