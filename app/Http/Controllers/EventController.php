<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Attendee;
use App\Event;
use App\Organiser;
use App\Page;
use App\Setting;

class EventController extends Controller
{
    /**
     * @var simply sets the path for the views in the CMS - makes it simpler to copy / paste new CRUD controllers
     */
	protected $crud = 'events';

    public function show($id)
    {
        if(! Auth::guard('exhibitor')->check()){
            return redirect('/oops/exhibitor');
        }

        $check = Attendee::where([['event_id', $id], ['exhibitor_id', Auth::guard('exhibitor')->user()->id]])->count();

        if($check==0){
            return redirect('/oops/exhibitor');
        }

        $page = [];
        $page['page'] = Page::where('slug', 'exhibitors')->first();
        $page['event'] = Event::where('id', $id)->first();
        $page['organiser'] = Organiser::where('id', $page['event']->organiser_id)->first();
        $page['page']->banner = $page['event']->image;
        $page['page']->title = $page['event']->event;
        $page['page']->caption = \Carbon\Carbon::parse($page['event']->start)->format('D jS M Y').' - '.\Carbon\Carbon::parse($page['event']->finish)->format('D jS M Y');
        $page['page']->link = '';
        $page['page']->link_text = '';

        $page['protected'] = 'events.index';
        $page['navigation'] = Page::where('menu', 1)->orderBy('title', 'asc')->get();
        $page['settings'] = Setting::all()->keyBy('ident');

        return view('view')->with('page', $page);
    }

    /**
     * Returns the admin view to a CMS user
     * @return type
     */
    public function admin()
    {
    	$page = [];
        $page['crud'] = Event::orderBy('start', 'asc')->get();
    	$page['view'] = 'crud.'.$this->crud.'.index';

        if(isset($page['crud'][0])){
            foreach($page['crud'] AS $event){
                $event->organiser = Organiser::where('id', $event->organiser_id)->first();
            }
        }

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the view to create a new crud item in the CMS
     * @return type
     */
    public function create()
    {
    	$page = [];
    	$page['organisers'] = Organiser::where('active', 1)->orderBy('organiser')->get();
    	$page['view'] = 'crud.'.$this->crud.'.create';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the form to update the crud item
     * @param type $id 
     * @return type
     */
    public function update($id)
    {
    	$page = [];
    	$page['crud'] = Event::where('id', $id)->first();
    	$page['organisers'] = Organiser::where('active', 1)->orderBy('organiser')->get();
    	$page['view'] = 'crud.'.$this->crud.'.update';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Updates or creates a crud item based on id
     * @param Request $request 
     * @return type
     */
    public function store(Request $request)
    {
        try{

        	$data = $request->except('_token');

            if(! empty($request->file('image'))){
                $data['image'] = Storage::put('public/events/images', $request->file('image'));
            }

            if(! empty($request->file('download_1'))){
                $data['download_1'] = Storage::put('public/events/downloads', $request->file('download_1'));
            }

            if(! empty($request->file('download_2'))){
                $data['download_2'] = Storage::put('public/events/downloads', $request->file('download_2'));
            }

            if(! empty($request->file('download_3'))){
                $data['download_3'] = Storage::put('public/events/downloads', $request->file('download_3'));
            }

            $type = Event::updateOrCreate(['id'=>$request->id],$data);

    	    if($type->wasRecentlyCreated){
    	    	$message = 'Record successfully created';
    	    } else{
    	    	$message = 'Record successfully updated';
    	    }
        } catch(\Exception $e){
            $message = 'Record could not be created, please make sure all required fields are filled out and that your slug is unique';
            dd($e);
            return redirect()->back()->with('danger', $message);
        }

    	return redirect('/admin/'.$this->crud)->with('success', $message);
    }

    /**
     * Returns the view to upload an import file
     * @return type
     */
    public function import()
    {
        $page = [];
        $page['events'] = Event::where('active', 1)->orderBy('event')->get();
        $page['view'] = 'crud.'.$this->crud.'.import';

        return view('admin.view')->with('page', $page);
    }

    /**
     * Returns a view of all exhibitors registered against the event - NB: The blade uses a facade to call the data in
     * @return type
     */
    public function exhibitors($id)
    {
        $page = [];
        $page['event'] = Event::where('id', $id)->first();
        $page['view'] = 'crud.'.$this->crud.'.exhibitors';

        return view('admin.view')->with('page', $page);
    }

    /**
     * Returns a view of all pending documents
     * @return type
     */
    public function documents($id)
    {
        $page = [];
        $page['event'] = Event::where('id', $id)->first();
        $page['view'] = 'crud.'.$this->crud.'.documents';

        return view('admin.view')->with('page', $page);
    }
}
