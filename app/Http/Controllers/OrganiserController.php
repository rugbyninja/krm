<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Organiser;
use App\Page;
use App\Setting;

class OrganiserController extends Controller
{
    /**
     * @var simply sets the path for the views in the CMS - makes it simpler to copy / paste new CRUD controllers
     */
	protected $crud = 'organisers';

    public function login(Request $request)
    {
        if(empty($request->login)){
            $page = [];
            $page['page'] = Page::where([['slug', 'organiser-login']])->first();
            if(! $page['page']){
                abort(404);
            }
            $page['navigation'] = Page::where('menu', 1)->orderBy('title', 'asc')->get();
            $page['settings'] = Setting::all()->keyBy('ident');
            $page['protected'] = 'organisers.login';
            return view('view')->with('page', $page)
                               ->with('request', $request);
        } else{
            $count = Event::where('email', $request->email)->count();
            if($count==0){
                return redirect()->back()->with('error', 'email');
            } else{
                return redirect('/organiser/dashboard')->cookie('organiser_email', $request->email, 1440);
            }
        }
    }

    /**
     * Returns the organisers dashboard view
     * @return type
     */
    public function index(Request $request)
    {
        $email = $request->cookie('organiser_email');

        $page = [];
        $page['page'] = Page::where('slug', 'organisers')->first();

        $page['events'] = Event::where('email', $email)->get();

        $page['protected'] = 'organisers.dashboard';
        $page['navigation'] = Page::where('menu', 1)->orderBy('title', 'asc')->get();
        $page['settings'] = Setting::all()->keyBy('ident');

        return view('view')->with('page', $page);
    }

    /**
     * Returns the event view to the organiser
     * @param type $id 
     * @return type
     */
    public function event($id, Request $request)
    {
        $email = $request->cookie('organiser_email');

        $page = [];
        $page['page'] = Page::where('slug', 'organisers')->first();
        $page['event'] = Event::where([['id', $id],['email', $email]])->first();

        if(! $page['event']){
            abort(404);
        }

        $page['organiser'] = Organiser::where('id', $page['event']->organiser_id)->first();
        $page['page']->banner = $page['event']->image;
        $page['page']->title = $page['event']->event;
        $page['page']->caption = \Carbon\Carbon::parse($page['event']->start)->format('D jS M Y').' - '.\Carbon\Carbon::parse($page['event']->finish)->format('D jS M Y');
        $page['page']->link = '';
        $page['page']->link_text = '';

        $page['protected'] = 'organisers.event';
        $page['navigation'] = Page::where('menu', 1)->orderBy('title', 'asc')->get();
        $page['settings'] = Setting::all()->keyBy('ident');

        return view('view')->with('page', $page);
    }

    /**
     * Returns the admin view to a CMS user
     * @return type
     */
    public function admin()
    {
    	$page = [];
        $page['crud'] = Organiser::orderBy('organiser')->get();
    	$page['view'] = 'crud.'.$this->crud.'.index';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the view to create a new crud item in the CMS
     * @return type
     */
    public function create()
    {
    	$page = [];
    	$page['view'] = 'crud.'.$this->crud.'.create';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the form to update the crud item
     * @param type $id 
     * @return type
     */
    public function update($id)
    {
    	$page = [];
    	$page['crud'] = Organiser::where('id', $id)->first();
    	$page['view'] = 'crud.'.$this->crud.'.update';

    	return view('admin.view')->with('page', $page);
    }

    /**
     * Updates or creates a crud item based on id
     * @param Request $request 
     * @return type
     */
    public function store(Request $request)
    {
        try{

        	$data = $request->except('_token');

            $type = Organiser::updateOrCreate(['id'=>$request->id],$data);

    	    if($type->wasRecentlyCreated){
    	    	$message = 'Record successfully created';
    	    } else{
    	    	$message = 'Record successfully updated';
    	    }
        } catch(\Exception $e){
            $message = 'Record could not be created, please make sure all required fields are filled out and that your slug is unique';
            return redirect()->back()->with('danger', $message);
        }

    	return redirect('/admin/'.$this->crud)->with('success', $message);
    }
}
            