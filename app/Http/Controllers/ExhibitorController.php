<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Attendee;
use App\Event;
use App\Exhibitor;
use App\Page;
use App\Setting;

class ExhibitorController extends Controller
{
    /**
     * @var simply sets the path for the views in the CMS - makes it simpler to copy / paste new CRUD controllers
     */
    protected $crud = 'exhibitors';

	/**
	 * Logs in the exhibitor based on id and hashed email address
	 * @param type $id 
	 * @param type $email 
	 * @return type
	 */
    public function login($id, $email)
    {
    	$exhibitor = Exhibitor::where('id', $id)->first();

    	$check = hash('md5',$exhibitor->email);

    	if($check==$email){
    		Auth::guard('exhibitor')->login($exhibitor);
    		return redirect('/exhibitor/dashboard');
    	} else{
		return redirect('/oops/exhibitor');
    	}
    }

    /**
     * Logsout the exhibitor and returns to a CMS controller page
     * @return type
     */
    public function logout()
    {
        Auth::guard('exhibitor')->logout();

        return redirect('/logout');
    }

    /**
     * Returns the exhibitors dashboard view
     * @return type
     */
    public function index()
    {
        if(! Auth::guard('exhibitor')->check()){
            return redirect('/exhibitor/login');
        }

		$page = [];
		$page['page'] = Page::where('slug', 'exhibitors')->first();
		$page['page']->title = Auth::guard('exhibitor')->user()->company;
        $page['page']->caption = 'Welcome to your account!';
        $page['page']->link = '';
        $page['page']->link_text = '';

        $attendees = Attendee::where('exhibitor_id', Auth::guard('exhibitor')->user()->id)->get();

        if(isset($attendees[0])){
            foreach($attendees AS $key => $attendee){
                $page['events'][$key] = Event::where('id', $attendee->event_id)->first();
            }
        } else{
            $page['events'] = null;
        }

        $page['protected'] = 'exhibitors.dashboard';
        $page['navigation'] = Page::where('menu', 1)->orderBy('title', 'asc')->get();
        $page['settings'] = Setting::all()->keyBy('ident');

        return view('view')->with('page', $page);
    }
    
    /**
     * Runs the import of the CSV / Excel file for all exhibitors
     * @param Request $request
     * @return type
     */
    public function import(Request $request)
    {
        $event = Event::where('id', $request->event_id)->first();
        $path = $request->file('import');
        $data = Excel::load($path, function($reader) {
        })->get();
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                try{
                    $create = [];
                    $create['company'] = $value->company;
                    $create['name'] = $value->name;
                    $create['phone'] = $value->phone;
                    $create['email'] = $value->email;
                    $create['address_1'] = $value->address;
                    $create['town'] = $value->town;
                    $create['postcode'] = $value->postcode;

                    $check = Exhibitor::where('company', $value->company)->orWhere('email', $value->email)->count();
                    if($check > 0){
                        Exhibitor::where('company', $value->company)->orWhere('email', $value->email)->update($create);
                    } else{
                        Exhibitor::create($create);
                    }
                    
                    $exhibitor = Exhibitor::where('company', $value->company)->first();
                    Attendee::register($event, $exhibitor, $value->stand, $value->dimensions);
                } catch(\Exception $e){

                }
            }
        }

        return redirect()->back()->with('success', 'Records successfully imported');
    }

    /**
     * Returns the admin view to a CMS user
     * @return type
     */
    public function admin()
    {
        $page = [];
        $page['crud'] = Exhibitor::orderBy('company', 'asc')->get();
        $page['view'] = 'crud.'.$this->crud.'.index';

        if(isset($page['crud'][0])){
            foreach($page['crud'] AS $exhibitor){
                $exhibitor->events = Attendee::join('events', 'attendees.event_id', '=', 'events.id')->where('exhibitor_id', $exhibitor->id)->get();
            }
        }

        return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the view to create a new crud item in the CMS
     * @return type
     */
    public function create()
    {
        $page = [];
        $page['events'] = Event::where('active', 1)->orderBy('event')->get();
        $page['view'] = 'crud.'.$this->crud.'.create';

        return view('admin.view')->with('page', $page);
    }

    /**
     * Returns the form to update the crud item
     * @param type $id 
     * @return type
     */
    public function update($id)
    {
        $page = [];
        $page['crud'] = Exhibitor::where('id', $id)->first();
        $page['crud']->events = Attendee::join('events', 'attendees.event_id', '=', 'events.id')->where('exhibitor_id', $page['crud']->id)->get();
        $page['events'] = Event::where('active', 1)->orderBy('event')->get();
        $page['view'] = 'crud.'.$this->crud.'.update';

        return view('admin.view')->with('page', $page);
    }

    /**
     * Updates or creates a crud item based on id
     * @param Request $request 
     * @return type
     */
    public function store(Request $request)
    {
        try{

            $data = $request->except('_token', 'event_id');

            $type = Exhibitor::updateOrCreate(['id'=>$request->id],$data);

            if(! empty($request->event_id)){
                $event = Event::where('id', $request->event_id)->first();
                Attendee::register($event, $type);
            }

            if($type->wasRecentlyCreated){
                $message = 'Record successfully created';
            } else{
                $message = 'Record successfully updated';
            }
        } catch(\Exception $e){
            $message = 'Record could not be created, please make sure all required fields are filled out and that your slug is unique';
            dd($e);
            return redirect()->back()->with('danger', $message);
        }

        return redirect('/admin/'.$this->crud)->with('success', $message);
    }

    public function documents($event, $id)
    {
        $page = [];
        $page['event'] = Event::where('id', $event)->first();
        $page['exhibitor'] = Exhibitor::where('id', $id)->first();
        $page['view'] = 'crud.'.$this->crud.'.documents';
        
        return view('admin.view')->with('page', $page);
    }
}