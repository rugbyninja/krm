<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendee;

class AttendeeController extends Controller
{
	/**
	 * Approves an exhibitor against a given event and triggers an email
	 * @param type $event 
	 * @param type $exhibitor 
	 * @return type
	 */
    public function approve($event, $exhibitor)
    {
    	return Attendee::approve($event, $exhibitor);
    }

	/**
	 * Approves all exhibitors against a given event that have submitted and had approved all mandatory documents and triggers an email
	 * @param type $event
	 * @return type
	 */
    public function approve_all($event)
    {
        return redirect()->back()->with('danger', 'Mass emails are currently disabled');
    	// return Attendee::approve_all($event);
    }

	/**
	 * Send an email informing all those that have missed the deadline for submitting documents
	 * @param type $event
	 * @return type
	 */
    public function deadline_all($event)
    {
        return redirect()->back()->with('danger', 'Mass emails are currently disabled');
    	// return Attendee::deadline_all($event);
    }

	/**
	 * Sends a reminder email to those yet to complete all documents
	 * @param type $event
	 * @return type
	 */
    public function reminder_all($event)
    {
        return redirect()->back()->with('danger', 'Mass emails are currently disabled');
    	// return Attendee::reminder_all($event);
    }
}
