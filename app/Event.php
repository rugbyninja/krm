<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $fillable = [
		'event', 'organiser_id', 'image', 'description', 'download_1', 'download_2', 'download_3', 'address_1', 'address_2', 'town', 'county', 'postcode', 'start', 'finish', 'documents', 'facebook', 'instagram', 'linkedin', 'twitter', 'website', 'active', 'download_1_name', 'download_2_name', 'download_3_name', 'email',
	];
}
