<div class="modal fade" id="{{ str_slug($document->name) }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form action="/exhibitor/upload" method="post" enctype="multipart/form-data">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ $document->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          {{ csrf_field() }}
          @if(! empty(Document::check($page['event']->id, Auth::guard('exhibitor')->user()->id, $document->name)->document[0]->notes))
          <div class="alert alert-info">{!! Document::check($page['event']->id, Auth::guard('exhibitor')->user()->id, $document->name)->document[0]->notes !!}</div>
          @endif
          @if(Document::check($page['event']->id, Auth::guard('exhibitor')->user()->id, $document->name)->bool)
          <input type="hidden" name="id" value="{{ Document::check($page['event']->id, Auth::guard('exhibitor')->user()->id, $document->name)->document[0]->id }}">
          <input type="hidden" name="status" value="Pending">
          @endif
          <input type="hidden" name="event" value="{{ $page['event']->id }}">
          <input type="hidden" name="type" value="{{ $document->name }}">
          <input type="file" name="document" required>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </form>
</div>