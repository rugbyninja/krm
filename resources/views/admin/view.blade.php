{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'KRM :: CMS')

@section('content')
@include('admin.partials.alert')
@include('admin.'.$page['view'])
@stop

@section('css')
@stop

@section('js')
<script src="/js/admin.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=1rrp584vjzmlt4o7r89n0vsrkvhdku8lszy6wxzecuuo9vjh"></script>
  <script>
  tinymce.init({
    selector: '#adminTextarea',
    height: 400,
  });
  </script>
@stop