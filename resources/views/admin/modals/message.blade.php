@if(isset($document))
<div class="modal fade" id="message{{ str_slug($document->name) }}" tabindex="-1" role="dialog" aria-labelledby="message{{ str_slug($document->name) }}" aria-hidden="true">
  <form action="/admin/document/message/{{ Document::check($page['event']->id, $page['exhibitor']->id,  $document->name)->document[0]->id }}" method="post" enctype="multipart/form-data">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="message{{ str_slug($document->name) }}">Message {{ $document->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          {{ csrf_field() }}
          <label>Message to exhibitor (required)</label>
          <textarea class="form-control" name="message" required></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </form>
</div>
@elseif(isset($item))
<div class="modal fade" id="message{{ str_slug($item->id) }}" tabindex="-1" role="dialog" aria-labelledby="message{{ str_slug($item->id) }}" aria-hidden="true">
  <form action="/admin/document/message/{{ Document::check($page['event']->id, $item->exhibitor_id,  $item->type)->document[0]->id }}" method="post" enctype="multipart/form-data">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="message{{ str_slug($item->type) }}">Message {{ $item->type }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          {{ csrf_field() }}
          <label>Message to exhibitor (required)</label>
          <textarea class="form-control" name="message" required></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </form>
</div>
@endif