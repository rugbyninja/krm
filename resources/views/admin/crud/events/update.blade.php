<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Events</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/events" class="btn btn-primary">All Events</a>
		</div>
	</div>
</div>

<div class="container">
	<form action="/admin/events/store" method="post" enctype="multipart/form-data" id="event">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $page['crud']->id }}">
		<div class="row well">
			<div class="col-md-6">
				<div class="form-group">
				<label>Event Name</label>
					<input type="text" name="event" class="form-control" value="{{ $page['crud']->event }}" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<label>Organiser</label>
					<select name="organiser_id" class="form-control" required>
						<option value="" selected disabled>Please choode an event organiser</option>
						@foreach($page['organisers'] AS $org)
						<option value="{{ $org->id }}"
						@if($page['crud']->organiser_id==$org->id)
						selected
						@endif
						>{{ $org->organiser }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<label>Event Start Date</label>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<input type="date" name="start" class="form-control" value="{{ $page['crud']->start }}" required>
				</div>
			</div>
			<div class="col-md-3">
				<label>Event Finish Date</label>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<input type="date" name="finish" class="form-control" value="{{ $page['crud']->finish }}" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Event Organiser Login Email</label>
					<input type="email" name="email" class="form-control" value="{{ $page['crud']->email }}" required>
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-6">
				<div class="form-group">
				<label>Address 1</label>
					<input type="text" name="address_1" class="form-control" value="{{ $page['crud']->address_1 }}" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<label>Address 2</label>
					<input type="text" name="address_2" class="form-control" value="{{ $page['crud']->address_2 }}" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Town / City</label>
					<input type="text" name="town" class="form-control" value="{{ $page['crud']->town }}" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Postcode</label>
					<input type="text" name="postcode" class="form-control" value="{{ $page['crud']->postcode }}" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>County</label>
					<input type="text" name="county" class="form-control" value="{{ $page['crud']->county }}" required>
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-4">
				<div class="form-group">
				<label>Facebook</label>
					<input type="text" name="facebook" class="form-control" value="{{ $page['crud']->facebook }}">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>LinkedIn</label>
					<input type="text" name="linkedin" class="form-control" value="{{ $page['crud']->linkedin }}">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Twitter</label>
					<input type="text" name="twitter" class="form-control" value="{{ $page['crud']->twitter }}">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Instagram</label>
					<input type="text" name="instagram" class="form-control" value="{{ $page['crud']->instagram }}">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Website</label>
					<input type="text" name="website" class="form-control" value="{{ $page['crud']->website }}">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<input type="checkbox" name="active" value="1"
					@if($page['crud']->active==1)
					checked
					@endif
					> Make event active?
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-3">
				<div class="form-group">
					<label>Event Image</label>
					<input type="file" name="image" class="imagePreview">
				</div>
			</div>
			<div class="col-md-9">
				<img id="imagePreview" width="100%" src="{{ Storage::url($page['crud']->image) }}">
			</div>
		</div>

		<div class="row well">
			<div class="col-md-4">
				<label>1st Useful Download</label>
				<input type="file" name="download_1">
				@if(! empty($page['crud']->download_1))
				<a href="{{ Storage::url($page['crud']->download_1) }}" download="{{ $page['crud']->download_1_name }}" style="text-decoration: none"><i class="fa fa-download" aria-hidden="true"></i> Download</a>
				@endif
			</div>
			<div class="col-md-4">
				<label>2nd Useful Download</label>
				<input type="file" name="download_2">
				@if(! empty($page['crud']->download_2))
				<a href="{{ Storage::url($page['crud']->download_2) }}" download="{{ $page['crud']->download_2_name }}" style="text-decoration: none"><i class="fa fa-download" aria-hidden="true"></i> Download</a>
				@endif
			</div>
			<div class="col-md-4">
				<label>3rd Useful Download</label>
				<input type="file" name="download_3">
				@if(! empty($page['crud']->download_3))
				<a href="{{ Storage::url($page['crud']->download_3) }}" download="{{ $page['crud']->download_3_name }}" style="text-decoration: none"><i class="fa fa-download" aria-hidden="true"></i> Download</a>
				@endif
			</div>
			<div class="col-md-4">
				<label>Download 1 Name</label>
				<input type="text" name="download_1_name" class="form-control" value="{{ $page['crud']->download_1_name }}">
			</div>
			<div class="col-md-4">
				<label>Download 2 Name</label>
				<input type="text" name="download_2_name" class="form-control" value="{{ $page['crud']->download_2_name }}">
			</div>
			<div class="col-md-4">
				<label>Download 3 Name</label>
				<input type="text" name="download_3_name" class="form-control" value="{{ $page['crud']->download_3_name }}">
			</div>
		</div>

		<div class="row well">
			<div class="col-md-10 eventdocs">
				@foreach(json_decode($page['crud']->documents) AS $document)
				<div class="col-md-12">
					<label>Document Name</label>
					<input type="text" class="form-control jsondocs-name" value="{{ $document->name }}">
					<label>Make mandatory?</label>
					<input type="checkbox" class="jsondocs-req" value="1"
					@if($document->required==1)
					checked
					@endif
					>
				</div>
				@endforeach
			</div>
			<div class="col-md-2">
				<span class="btn btn-success newdoc"><i class="fa fa-plus" aria-hidden="true"></i></span>
			</div>
		</div>
		<input type="hidden" name="documents" value="{{ $page['crud']->documents }}" required>
		<div class="row well">
			<div class="col-md-12">
				<label>Description</label>
				<textarea id="adminTextarea" name="description" class="form-control">{!! $page['crud']->description !!}</textarea>
			</div>
		</div>
		<button type="submit" class="btn btn-success">Update Event</button>
	</form>
</div>