<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Events</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/events" class="btn btn-primary">All Events</a>
		</div>
	</div>
</div>

<div class="container">
	<form action="/admin/exhibitors/import" method="post" enctype="multipart/form-data" id="event">
		{{ csrf_field() }}
		<div class="row well">
			<div class="col-md-3">
				<div class="form-group">
				<label>Upload File</label>
					<input type="file" name="import" required>
				</div>
			</div>
			<div class="col-md-9">
				<div class="form-group">
				<label>Event</label>
					<select name="event_id" class="form-control" required>
						<option selected disabled>Please choode an event</option>
						@foreach($page['events'] AS $event)
						<option value="{{ $event->id }}">{{ $event->event }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<button type="submit" class="btn btn-success">Run Import</button>
	</form>
</div>