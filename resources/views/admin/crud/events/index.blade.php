<div class="container-flex">
	<div class="row">
		<div class="col-md-10">
			<h2>Events</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/events/create" class="btn btn-success">New Event</a>
		</div>
		<div class="col-md-1">
			<a href="/admin/events/import" class="btn btn-success">Import Exhibitors</a>
		</div>
	</div>
</div>

<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover well">
				<thead>
					<tr>
						<th>Event</th>
						<th>Organiser</th>
						<th>Location</th>
						<th>Date</th>
						<th>Documents</th>
						<th>Active</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach($page['crud'] AS $item)
				<tr>
					<td>{{ $item->event }}</td>
					<td>{{ $item->organiser->organiser }}</td>
					<td>{{ $item->address_1 }} {{ $item->address_2 }} {{ $item->town }} {{ $item->county }} {{ $item->postcode }}</td>
					<td style="width: 15%;">
						<strong>Start: </strong>{{ $item->start }}<br>
						<strong>Finish: </strong>{{ $item->finish }}
					</td>
					<td>
						@foreach(json_decode($item->documents) AS $doc)
						<li>{{ $doc->name }}
							@if($doc->required==1)
							<label class="label label-success">mandatory</label>
							@else
							<label class="label label-info">optional</label>
							@endif
						</li>
						@endforeach
					</td>
					<td>
						@if($item->active==1)
							<span class="label label-success">Active</span>
						@else
							<span class="label label-danger">Inactive</span>
						@endif
					</td>
					<td>
						<a class="btn btn-primary" href="/admin/events/documents/{{ $item->id }}"><i class="fa fa-file-text"></i></a>
						<a class="btn btn-primary" href="/admin/events/exhibitors/{{ $item->id }}"><i class="fa fa-users"></i></a>
						<a class="btn btn-primary" href="/admin/events/update/{{ $item->id }}"><i class="fa fa-eye"></i></a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>