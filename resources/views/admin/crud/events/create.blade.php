<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Events</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/events" class="btn btn-primary">All Events</a>
		</div>
	</div>
</div>

<div class="container">
	<form action="/admin/events/store" method="post" enctype="multipart/form-data" id="event">
		{{ csrf_field() }}
		<div class="row well">
			<div class="col-md-6">
				<div class="form-group">
				<label>Event Name</label>
					<input type="text" name="event" class="form-control" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<label>Organiser</label>
					<select name="organiser_id" class="form-control" required>
						<option value="" selected disabled>Please choode an event organiser</option>
						@foreach($page['organisers'] AS $org)
						<option value="{{ $org->id }}">{{ $org->organiser }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<label>Event Start Date</label>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<input type="date" name="start" class="form-control" required>
				</div>
			</div>
			<div class="col-md-3">
				<label>Event Finish Date</label>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<input type="date" name="finish" class="form-control" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Event Organiser Login Email</label>
					<input type="email" name="email" class="form-control" required>
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-6">
				<div class="form-group">
				<label>Address 1</label>
					<input type="text" name="address_1" class="form-control" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<label>Address 2</label>
					<input type="text" name="address_2" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Town / City</label>
					<input type="text" name="town" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Postcode</label>
					<input type="text" name="postcode" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>County</label>
					<input type="text" name="county" class="form-control" required>
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-4">
				<div class="form-group">
				<label>Facebook</label>
					<input type="text" name="facebook" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>LinkedIn</label>
					<input type="text" name="linkedin" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Twitter</label>
					<input type="text" name="twitter" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Instagram</label>
					<input type="text" name="instagram" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Website</label>
					<input type="text" name="website" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<input type="checkbox" name="active" value="1" checked> Make event active?
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-3">
				<div class="form-group">
					<label>Event Image</label>
					<input type="file" name="image" class="imagePreview" required>
				</div>
			</div>
			<div class="col-md-9">
				<img id="imagePreview" width="100%">
			</div>
		</div>

		<div class="row well">
			<div class="col-md-4">
				<label>1st Useful Download</label>
				<input type="file" name="download_1">
			</div>
			<div class="col-md-4">
				<label>2nd Useful Download</label>
				<input type="file" name="download_2">
			</div>
			<div class="col-md-4">
				<label>3rd Useful Download</label>
				<input type="file" name="download_3">
			</div>
			<div class="col-md-4">
				<label>Download 1 Name</label>
				<input type="text" name="download_1_name" class="form-control">
			</div>
			<div class="col-md-4">
				<label>Download 2 Name</label>
				<input type="text" name="download_2_name" class="form-control">
			</div>
			<div class="col-md-4">
				<label>Download 3 Name</label>
				<input type="text" name="download_3_name" class="form-control">
			</div>
		</div>

		<div class="row well">
			<div class="col-md-10 eventdocs">
				<div class="col-md-12">
					<label>Document Name</label>
					<input type="text" class="form-control jsondocs-name">
					<label>Make mandatory?</label>
					<input type="checkbox" class="jsondocs-req" val="1">
				</div>
			</div>
			<div class="col-md-2">
				<span class="btn btn-success newdoc"><i class="fa fa-plus" aria-hidden="true"></i></span>
			</div>
		</div>
		<input type="hidden" name="documents" required>
		<div class="row well">
			<div class="col-md-12">
				<label>Description</label>
				<textarea id="adminTextarea" name="notes" class="form-control"></textarea>
			</div>
		</div>
		<button type="submit" class="btn btn-success">Create Event</button>
	</form>
</div>