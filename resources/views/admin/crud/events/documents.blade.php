<div class="container-flex">
	<div class="row">
		<div class="col-md-8">
			<h2>{{ $page['event']->event }} - All Documents</h2>
		</div>
		<div class="col-md-4">
			<a href="/admin/events/approve/{{ $page['event']->id }}" class="btn btn-success"><i class="fa fa-envelope-o"></i> Permissions and Reminder</a><br>
			<a href="/admin/events/deadline/{{ $page['event']->id }}" class="btn btn-warning"><i class="fa fa-envelope-o"></i> Permissions and Deadline Missed</a><br>
			<a href="/admin/events" class="btn btn-primary">All Events</a>
		</div>
	</div>
</div>
{{ Document::pending($page['event']->id)->links() }}
<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover well">
				<thead>
					<tr>
						<th>Exhibitor</th>
						<th>Type</th>
						<th>Status</th>
						<th>Notes</th>
						<th>Download</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>	
				@foreach(Document::pending($page['event']->id) AS $item)
				<tr>
					<td>{!! Exhibitor::user($item->exhibitor_id)->company !!}</td>
					<td>{!! $item->type !!}</td>
					<td>{{ $item->status }}</td>
					<td>{!! $item->notes !!}</td>
					<td></td>
					<td>
						@if(Document::check($page['event']->id, $item->exhibitor_id, $item->type)->bool)
						<a class="btn btn-primary" href="/admin/download/{{ Document::check($page['event']->id, $item->exhibitor_id,  $item->type)->document[0]->id }}"><i class="fa fa-download"></i></a>
						@include('admin.modals.approve')
						<a class="btn btn-success" data-toggle="modal" data-target="#approve{{ str_slug($item->id) }}"><i class="fa fa-check"></i></a>
						@include('admin.modals.decline')
						<a class="btn btn-danger" data-toggle="modal" data-target="#decline{{ str_slug($item->id) }}"><i class="fa fa-times"></i></a>
						@include('admin.modals.message')
						<a class="btn btn-warning" data-toggle="modal" data-target="#message{{ str_slug($item->id) }}"><i class="fa fa-envelope"></i></a>
						@endif
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>