<div class="container-flex">
	<div class="row">
		<div class="col-md-8">
			<h2>{{ $page['event']->event }}</h2>
		</div>
		<div class="col-md-4">
			<a href="/admin/events/approve/{{ $page['event']->id }}" class="btn btn-success"><i class="fa fa-envelope-o"></i> Permission to Build</a><br>
			<a href="/admin/events/reminder/{{ $page['event']->id }}" class="btn btn-warning"><i class="fa fa-envelope-o"></i> Reminder Email</a><br>
			<a href="/admin/events/deadline/{{ $page['event']->id }}" class="btn btn-danger"><i class="fa fa-envelope-o"></i> Deadline Missed</a><br>
			<a href="/admin/events" class="btn btn-primary">All Events</a>
		</div>
	</div>
</div>
<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover well">
				<thead>
					<tr>
						<th>Name</th>
						<th>Company</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>	
				@foreach(Exhibitor::event($page['event']->id) AS $item)
				<tr>
					<td>{{ $item->name }}</td>
					<td>{{ $item->company }}</td>
					<td>
						@if(count(Document::exhibitor($page['event']->id, $item->exhibitor_id)) > 0)
						<span class="btn btn-success">Documents Submitted <span class="badge">{{ count(Document::exhibitor($page['event']->id, $item->exhibitor_id)) }}</span></span>
						@else
						<span class="btn btn-danger">Documents Submitted <span class="badge">0</span></span>
						@endif					
					</td>
					<td>
						<a class="btn btn-success" href="/admin/exhibitors/approve/{{ $page['event']->id }}/{{ $item->exhibitor_id }}">Send Permission to Build</a>
						<a class="btn btn-primary" href="/admin/exhibitors/documents/{{ $page['event']->id }}/{{ $item->exhibitor_id }}"><i class="fa fa-eye"></i></a>
						@if(Attendee::status($page['event']->id, $item->exhibitor_id)->bool)
						@if(Attendee::permission($page['event']->id, $item->exhibitor_id))
						<a class="btn btn-success" href="/admin/exhibitors/approve/{{ $page['event']->id }}/{{ $item->exhibitor_id }}">Resend permission to build</a>
						@else
						<a class="btn btn-success" href="/admin/exhibitors/approve/{{ $page['event']->id }}/{{ $item->exhibitor_id }}">Permission to build</a>
						@endif
						@endif
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>