<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Organisers</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/organisers/create" class="btn btn-success">New Organiser</a>
		</div>
	</div>
</div>

<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover well">
				<thead>
					<tr>
						<th>Organiser</th>
						<th>Contact</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Active</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach($page['crud'] AS $item)
				<tr>
					<td>{{ $item->organiser }}</td>
					<td>{{ $item->name }}</td>
					<td>{{ $item->phone }}</td>
					<td>{{ $item->email }}</td>
					<td>
						@if($item->active==1)
							<span class="label label-success">Active</span>
						@else
							<span class="label label-danger">Inactive</span>
						@endif
					</td>
					<td>
						<a class="btn btn-primary" href="/admin/organisers/update/{{ $item->id }}"><i class="fa fa-eye"></i></a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>