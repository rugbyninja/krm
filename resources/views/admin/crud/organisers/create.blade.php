<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Organsiers</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/organisers" class="btn btn-primary">All Organisers</a>
		</div>
	</div>
</div>

<div class="container">
	<form action="/admin/organisers/store" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row well">
			<div class="col-md-12">
				<div class="form-group">
				<label>Organiser / Company Name</label>
					<input type="text" name="organiser" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Contact Name</label>
					<input type="text" name="name" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Email</label>
					<input type="email" name="email" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Phone</label>
					<input type="text" name="phone" class="form-control" required>
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-6">
				<div class="form-group">
				<label>Address 1</label>
					<input type="text" name="address_1" class="form-control" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<label>Address 2</label>
					<input type="text" name="address_2" class="form-control" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<label>Town / City</label>
					<input type="text" name="town" class="form-control" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<label>Postcode</label>
					<input type="text" name="postcode" class="form-control" required>
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-4">
				<div class="form-group">
				<label>Facebook</label>
					<input type="text" name="facebook" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>LinkedIn</label>
					<input type="text" name="linkedin" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Twitter</label>
					<input type="text" name="twitter" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Instagram</label>
					<input type="text" name="instagram" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Website</label>
					<input type="text" name="website" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<input type="checkbox" name="active" value="1" checked> Make organiser active?
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-12">
				<label>Notes</label>
				<textarea id="adminTextarea" name="notes" class="form-control"></textarea>
			</div>
		</div>
		<button type="submit" class="btn btn-success">Create Organiser</button>
	</form>
</div>