<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Pages</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/pages/create" class="btn btn-success">New Page</a>
		</div>
	</div>
</div>

<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover well">
				<thead>
					<tr>
						<th>Title</th>
						<th>Slug</th>
						<th>Content Preview</th>
						<th>Active</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach($page['crud'] AS $item)
				<tr>
					<td>{{ $item->title }}</td>
					<td><a href="/{{ $item->slug }}" target="_blank">/{{ $item->slug }}</a></td>
					<td>{{ str_limit(strip_tags($item->content), 100, '...')}}</td>
					<td>
						@if($item->active==1)
							<span class="label label-success">Active</span>
						@else
							<span class="label label-danger">Inactive</span>
						@endif
					</td>
					<td>
						<a href="/admin/pages/update/{{ $item->id }}"><i class="fa fa-eye"></i></a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>