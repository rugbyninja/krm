<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Pages</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/pages" class="btn btn-primary">All Pages</a>
		</div>
	</div>
</div>

<div class="container">
	<form action="/admin/pages/store" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row well">
			<div class="col-md-2">
				<div class="row">
					<div class="col-md-12">
						<input type="checkbox" name="menu" value="1">
						Include in menu?
					</div>
					<div class="col-md-12">
						<input type="checkbox" name="auth" value="1">
						All logged in users?
					</div>
					<div class="col-md-12">
						<input type="checkbox" name="exhibitor" value="1">
						Exhibitors only?
					</div>
					<div class="col-md-12">
						<input type="checkbox" name="organiser" value="1">
						Organisers only?
					</div>
					<div class="col-md-12">
						<input type="checkbox" name="active" value="1" checked>
						Make page active?
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<label>Page Title</label>
				<input type="text" name="title" class="form-control" required>
			</div>
			<div class="col-md-5">
				<label>Page Slug (must be unique)</label>
				<input type="slug" name="slug" class="form-control" required>
			</div>
		</div>
		<div class="row well">
			<div class="col-md-3">
				<div class="col-md-12">
					<label>Banner Image</label>
					<input type="file" name="banner" class="imagePreview" required>
				</div>
				<div class="col-md-12">
					<label>Banner Caption</label>
					<input type="text" name="caption" class="form-control" placeholder="The subtitle / caption underneath the title...">
				</div>
				<div class="col-md-12">
					<label>Banner Link</label>
					<input type="text" name="link" class="form-control" placeholder="http://...">
				</div>
				<div class="col-md-12">
					<label>Banner Link Text</label>
					<input type="text" name="link_text" class="form-control" placeholder="Click Here!">
				</div>
				<div class="col-md-12">
				</div>
			</div>
			<div class="col-md-9">
				<img id="imagePreview" width="100%">
			</div>
		</div>
		<div class="row well">
			<div class="col-md-12">
				<label>Page Content</label>
				<textarea id="adminTextarea" name="content" class="form-control"></textarea>
			</div>
		</div>
		<button type="submit" class="btn btn-success">Create Page</button>
	</form>
</div>