<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Pages</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/pages" class="btn btn-primary">All Pages</a>
		</div>
	</div>
</div>

<div class="container">
	<form action="/admin/pages/store" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $page['crud']->id }}">
		<div class="row well">
			<div class="col-md-2">
				<div class="row">
					<div class="col-md-12">
						<input type="checkbox" name="menu" value="1"
						@if($page['crud']->menu==1)
						checked
						@endif
						>
						Include in menu?
					</div>
					<div class="col-md-12">
						<input type="checkbox" name="auth" value="1"
						@if($page['crud']->auth==1)
						checked
						@endif
						>
						All logged in users?
					</div>
					<div class="col-md-12">
						<input type="checkbox" name="exhibitor" value="1"
						@if($page['crud']->exhibitor==1)
						checked
						@endif
						>
						Exhibitors only?
					</div>
					<div class="col-md-12">
						<input type="checkbox" name="organiser" value="1"
						@if($page['crud']->organiser==1)
						checked
						@endif
						>
						Organisers only?
					</div>
					<div class="col-md-12">
						<input type="checkbox" name="active" value="1"
						@if($page['crud']->active==1)
						checked
						@endif
						>
						Make page active?
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<label>Page Title</label>
				<input type="text" name="title" class="form-control" value="{{ $page['crud']->title }}" required>
			</div>
			<div class="col-md-5">
				<label>Page Slug</label><span class="small"> WARNING - Changing this can break links in your site and must be unique</span>
				<input type="slug" name="slug" class="form-control" value="{{ $page['crud']->slug }}" required>
			</div>
		</div>
		<div class="row well">
			<div class="col-md-3">
				<div class="col-md-12">
					<label>Banner Image</label>
					<input type="file" name="banner" class="imagePreview">
				</div>
				<div class="col-md-12">
					<label>Banner Caption</label>
					<input type="text" name="caption" class="form-control" placeholder="The subtitle / caption underneath the title..." value="{{ $page['crud']->caption }}">
				</div>
				<div class="col-md-12">
					<label>Banner Link</label>
					<input type="text" name="link" class="form-control" placeholder="http://..." value="{{ $page['crud']->link }}">
				</div>
				<div class="col-md-12">
					<label>Banner Link Text</label>
					<input type="text" name="link_text" class="form-control" placeholder="Click Here!" value="{{ $page['crud']->link_text }}">
				</div>
				<div class="col-md-12">
				</div>
			</div>
			<div class="col-md-9">
				<img id="imagePreview" width="100%" src="{{ Storage::url($page['crud']->banner) }}">
			</div>
		</div>
		<div class="row well">
			<div class="col-md-12">
				<label>Page Content</label>
				<textarea id="adminTextarea" name="content" class="form-control">{!! $page['crud']->content !!}</textarea>
			</div>
		</div>
		<button type="submit" class="btn btn-success">Update Page</button>
	</form>
</div>