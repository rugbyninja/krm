<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Exhibitors</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/exhibitors/create" class="btn btn-success">New Exhibitor</a>
		</div>
	</div>
</div>

<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover well">
				<thead>
					<tr>
						<th>Exhibitor</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Events</th>
						<th>Active</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach($page['crud'] AS $item)
				<tr>
					<td>{{ $item->company }}</td>
					<td>{{ $item->name }}</td>
					<td>{{ $item->phone }}</td>
					<td>{{ $item->email }}</td>
					<td>
						@if(isset($item->events[0]))
						@foreach($item->events AS $event)
						<li>{{ $event->event }}
						@endforeach
						@else
						<p>No registered events</p>
						@endif
					</td>
					<td>
						@if($item->active==1)
							<span class="label label-success">Active</span>
						@else
							<span class="label label-danger">Inactive</span>
						@endif
					</td>
					<td>
						<a class="btn btn-primary" href="/admin/exhibitors/update/{{ $item->id }}"><i class="fa fa-eye"></i></a>
						<a class="btn btn-info" href="{{ env('APP_URL') }}/exhibitor/login/{{ $item->id }}/{{ hash('md5', $item->email) }}" target="_blank"><i class="fa fa-user-secret"></i></a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>