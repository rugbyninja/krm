<div class="container-flex">
	<div class="row">
		<div class="col-md-10">
			<h2>{{ $page['event']->event }} - {{ $page['exhibitor']->company }}</h2>
		</div>
		<div class="col-md-2">
			<a href="/admin/events/exhibitors/{{ $page['event']->id }}" class="btn btn-primary">All Exhibitors</a>
		</div>
	</div>
</div>
<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover well">
				<thead>
					<tr>
						<th>Document</th>
						<th>Required</th>
						<th>Status</th>
						<th>Notes</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>	
				@foreach(json_decode($page['event']->documents) AS $document)
				<tr>
					<td>{{ $document->name }}</td>
					<td>
						@if($document->required==1)
						Required
						@else
						Optional
						@endif
					</td>
					<td>
						@if(Document::check($page['event']->id, $page['exhibitor']->id, $document->name)->bool)
						{{ Document::check($page['event']->id, $page['exhibitor']->id, $document->name)->document[0]->status }}
						@else
						Not Submitted
						@endif
					</td>
					<td>
						@if(Document::check($page['event']->id, $page['exhibitor']->id, $document->name)->bool)
						{!! nl2br(Document::check($page['event']->id, $page['exhibitor']->id, $document->name)->document[0]->notes) !!}
						@endif
					</td>
					<td>
						@if(Document::check($page['event']->id, $page['exhibitor']->id, $document->name)->bool)
						<a class="btn btn-primary" href="/admin/download/{{ Document::check($page['event']->id, $page['exhibitor']->id,  $document->name)->document[0]->id }}"><i class="fa fa-download"></i></a>
						@include('admin.modals.approve')
						<a class="btn btn-success" data-toggle="modal" data-target="#approve{{ str_slug($document->name) }}"><i class="fa fa-check"></i></a>
						@include('admin.modals.decline')
						<a class="btn btn-danger" data-toggle="modal" data-target="#decline{{ str_slug($document->name) }}"><i class="fa fa-times"></i></a>
						@include('admin.modals.message')
						<a class="btn btn-warning" data-toggle="modal" data-target="#message{{ str_slug($document->name) }}"><i class="fa fa-envelope"></i></a>
						@else
						<a class="btn btn-success" href="/admin/event/{{ $page['event']->id }}/{{ $page['exhibitor']->id }}/{{ $document->name }}"><i class="fa fa-check"></i></a>
						@endif
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>