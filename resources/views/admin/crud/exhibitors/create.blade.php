documents
<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Exhibitors</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/exhibitors" class="btn btn-primary">All Exhibitors</a>
		</div>
	</div>
</div>

<div class="container">
	<form action="/admin/exhibitors/store" method="post" enctype="multipart/form-data" id="exhibitor">
		{{ csrf_field() }}
		<div class="row well">
			<div class="col-md-6">
				<div class="form-group">
				<label>Exhibitor / Company Name (must be unique)</label>
					<input type="text" name="company" class="form-control" required>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				<label>Event</label>
					<select name="event_id" class="form-control" required>
						<option value="" selected disabled>Please choode an event</option>
						@foreach($page['events'] AS $event)
						<option value="{{ $event->id }}">{{ $event->event }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-4">
				<div class="form-group">
				<label>Contact Name</label>
					<input type="text" name="name" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Email (must be unique)</label>
					<input type="email" name="email" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Phone</label>
					<input type="text" name="phone" class="form-control" required>
				</div>
			</div>
		</div>

		<div class="row well">
			<div class="col-md-4">
				<div class="form-group">
				<label>Address 1</label>
					<input type="text" name="address_1" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Town / City</label>
					<input type="text" name="town" class="form-control" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
				<label>Postcode</label>
					<input type="text" name="postcode" class="form-control" required>
				</div>
			</div>
		</div>
		<div class="row well">
			<div class="col-md-12">
				<div class="form-group">
					<input type="checkbox" name="active" value="1" checked> Make account active?
				</div>
			</div>
		</div>
		<button type="submit" class="btn btn-success">Create Exhibitor</button>
	</form>
</div>