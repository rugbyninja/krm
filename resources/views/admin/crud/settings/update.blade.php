<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Settings</h2>
		</div>
		<div class="col-md-1">
			<a href="/admin/settings" class="btn btn-primary">All Settings</a>
		</div>
	</div>
</div>

<div class="container">
	<form action="/admin/settings/store" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $page['crud']->id }}">
		<div class="row well">
			<div class="col-md-12">
				<label>{{ $page['crud']->title }}</label>
				@if($page['crud']->ident=='APPROVE' || $page['crud']->ident=='REMINDER' || $page['crud']->ident=='DEADLINE')
				<textarea name="value" id="adminTextarea" class="form-control">{!! $page['crud']->value !!}</textarea>
				@else
				<textarea name="value" class="form-control">{!! $page['crud']->value !!}</textarea>
				@endif
			</div>
		</div>
		<button type="submit" class="btn btn-success">Update Setting</button>
	</form>
</div>