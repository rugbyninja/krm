<div class="container-flex">
	<div class="row">
		<div class="col-md-11">
			<h2>Site Settings</h2>
		</div>
	</div>
</div>

<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover well">
				<thead>
					<tr>
						<th>Setting</th>
						<th>Value</th>
						<th>Actions</th>
				</thead>
				<tbody>
				@foreach($page['crud'] AS $item)
				<tr>
					<td>{{ $item->title }}</td>
					<td>{!! nl2br($item->value) !!}</td>
					<td>
						<a class="btn btn-primary" href="/admin/settings/update/{{ $item->id }}"><i class="fa fa-eye"></i></a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>