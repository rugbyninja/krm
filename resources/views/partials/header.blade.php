	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="/img/fav.png">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>{{ $page['page']->title }}</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="/css/linearicons.css">
			<link rel="stylesheet" href="/css/font-awesome.min.css">
			<link rel="stylesheet" href="/css/bootstrap.css">
			<link rel="stylesheet" href="/css/magnific-popup.css">
			<link rel="stylesheet" href="/css/nice-select.css">							
			<link rel="stylesheet" href="/css/animate.min.css">
			<link rel="stylesheet" href="/css/owl.carousel.css">			
			<link rel="stylesheet" href="/css/jquery-ui.css">			
			<link rel="stylesheet" href="/css/main.css">
		</head>
		<body>
		  <header id="header" id="home">
	  		<div class="header-top">
	  			<div class="container">
			  		<div class="row">
			  			<div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">
			  				<ul>
			  					@if(! empty($page['settings']['facebook']->value))
								<li><a href="{{ $page['settings']['facebook']->value }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
								@endif
			  					@if(! empty($page['settings']['twitter']->value))
								<li><a href="{{ $page['settings']['twitter']->value }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
								@endif
			  					@if(! empty($page['settings']['linkedin']->value))
								<li><a href="{{ $page['settings']['linkedin']->value }}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
								@endif
			  				</ul>			
			  			</div>
			  			<div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
			  				<a href="tel:{{ $page['settings']['phone']->value }}"><span class="lnr lnr-phone-handset"></span> <span class="text">{{ $page['settings']['phone']->value }}</span></a>
			  				<a href="{{ $page['settings']['email']->value }}"><span class="lnr lnr-envelope"></span> <span class="text">{{ $page['settings']['email']->value }}</span></a>			
			  			</div>
			  		</div>			  					
	  			</div>
			</div>
			@include('partials.navigation')
		  </header>
			@if(! empty($page['page']->link))
			<section class="banner-area relative" id="home" style="background: url('{{ Storage::url($page['page']->banner) }}'); background-size: cover;">
				<div class="overlay overlay-bg"></div>	
				<div class="container">
					<div class="row fullscreen d-flex align-items-center justify-content-between">
						<div class="banner-content col-lg-9 col-md-12">
							<h1 class="text-uppercase">
								{{ $page['page']->title }}			
							</h1>
							<p class="pt-10 pb-10">
								{{ $page['page']->caption }}
							</p>
							<a href="{{ $page['page']->link }}" class="primary-btn text-uppercase">{{ $page['page']->link_text }}</a>
						</div>						
					</div>
				</div>
			</section>
			@else
			<section class="banner-area relative about-banner" id="home" style="background: url('{{ Storage::url($page['page']->banner) }}'); background-size: cover;">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								{{ $page['page']->title }}
							</h1>
							<p class="pt-10 pb-10">
								{{ $page['page']->caption }}
							</p>
						</div>	
					</div>
				</div>
			</section>
			@endif