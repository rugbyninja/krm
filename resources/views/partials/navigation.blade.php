<div class="container main-menu">
	<div class="row align-items-center justify-content-between d-flex">
		<div id="logo">
			<a href="/"><img src="/img/logo.png" alt="" title="" height="75"/></a>
		</div>
		<nav id="nav-menu-container">
			<ul class="nav-menu">
				@foreach($page['navigation'] AS $nav)
				<li><a href="/{{ $nav->slug }}">{{ $nav->title }}</a></li>
				@endforeach
				@if(Auth::guard('exhibitor')->check())
				<li><a href="/exhibitor/logout">Logout</a></li>
				@endif
			</ul>
		</nav>		
	</div>
</div>