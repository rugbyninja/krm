						
			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="footer-bottom row">
						<div class="col-lg-6 col-sm-6 footer-social">
							@if(! empty($page['settings']['facebook']->value))
							<a href="{{ $page['settings']['facebook']->value }}" target="_blank"><i class="fa fa-facebook"></i></a>
							@endif
							@if(! empty($page['settings']['twitter']->value))
							<a href="{{ $page['settings']['twitter']->value }}" target="_blank"><i class="fa fa-twitter"></i></a>
							@endif
							@if(! empty($page['settings']['linkedin']->value))
							<a href="{{ $page['settings']['linkedin']->value }}" target="_blank"><i class="fa fa-linkedin"></i></a>
							@endif
						</div>
						<div class="col-lg-6 col-sm-6 footer-social">
							&copy; KRM Safety 2019 - all rights reserved
						</div>
					</div>						
				</div>
			</footer>	
			<!-- End footer Area -->	

			<script src="/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="/js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="/js/easing.min.js"></script>			
			<script src="/js/hoverIntent.js"></script>
			<script src="/js/superfish.min.js"></script>	
			<script src="/js/jquery.ajaxchimp.min.js"></script>
			<script src="/js/jquery.magnific-popup.min.js"></script>	
    		<script src="/js/jquery.tabs.min.js"></script>						
			<script src="/js/jquery.nice-select.min.js"></script>	
			<script src="/js/owl.carousel.min.js"></script>									
			<script src="/js/mail-script.js"></script>	
			<script src="/js/main.js"></script>
		</body>
	</html>