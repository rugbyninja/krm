<div class="container py-3">
	<div class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-4">
		@if(Session::has('error'))
		<div class="alert alert-danger">
			Sorry, that email address was not recognised, pelase try again or contact us for more informaiton
		</div>
		@endif
			<form action="" method="post">
				{{ csrf_field() }}
				<input type="hidden" name="login" value="1">
				<div class="form-group">
					<label>Email Address</label>
					<input type="email" name="email" class="form-control" required>
				</div>
				<button type="submit" class="btn btn-success">Login</button>
			</form>
		</div>
		<div class="col-md-4">
		</div>
	</div>
</div>