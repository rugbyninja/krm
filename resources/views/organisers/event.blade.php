			<div class="container py-5">
				<div class="row">
					<div class="col-md-4">
						<h3>Documents Submitted</h3>
						<canvas id="submitted" width="400" height="400"></canvas>
					</div>
					<div class="col-md-4">
						<h3>Documents Approved</h3>
						<canvas id="approved" width="400" height="400"></canvas>
					</div>
					<div class="col-md-4">
						<h3>Event Completion</h3>
						<canvas id="completion" width="400" height="400"></canvas>
					</div>
				</div>
			</div>
			<!-- Start event-details Area -->
			<section class="event-details-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 event-details-left">
							<div class="main-img">
								<img class="img-fluid" src="{{ Storage::url($page['event']->image) }}" alt="">
							</div>
							<div class="details-content">
								{!! $page['event']->description !!}
							</div>
							<div class="social-nav row no-gutters">
								<div class="col-lg-6 col-md-6 ">
									<ul class="focials">
										@if(! empty($page['event']->facebook))
										<li><a href="{{ $page['event']->facebook }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
										@endif
										@if(! empty($page['event']->twitter))
										<li><a href="{{ $page['event']->twitter }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
										@endif
										@if(! empty($page['event']->linkedin))
										<li><a href="{{ $page['event']->linkedin }}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
										@endif
										@if(! empty($page['event']->instagram))
										<li><a href="{{ $page['event']->instagram }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
										@endif
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-4 event-details-right">
							@if(! empty($page['event']->download_1) || ! empty($page['event']->download_2 || ! empty($page['download_3'])))
							<div class="single-event-details">
								<h4>Useful Downloads</h4>
								<ul class="mt-10">
									@if(! empty($page['event']->download_1))
									<a href="{{ Storage::url($page['event']->download_1) }}" download="{{ $page['event']->download_1_name }}" style="text-decoration: none">
										<li class="justify-content-between d-flex">
											<span>{{ $page['event']->download_1_name }}</span>
											<span><i class="fa fa-download" aria-hidden="true"></i></span>
										</li>
									</a>
									@endif
									@if(! empty($page['event']->download_2))
									<a href="{{ Storage::url($page['event']->download_2) }}" download="{{ $page['event']->download_2_name }}" style="text-decoration: none">
										<li class="justify-content-between d-flex">
											<span>{{ $page['event']->download_2_name }}</span>
											<span><i class="fa fa-download" aria-hidden="true"></i></span>
										</li>
									</a>
									@endif	
									@if(! empty($page['event']->download_3))
									<a href="{{ Storage::url($page['event']->download_3) }}" download="{{ $page['event']->download_3_name }}" style="text-decoration: none">
										<li class="justify-content-between d-flex">
											<span>{{ $page['event']->download_3_name }}</span>
											<span><i class="fa fa-download" aria-hidden="true"></i></span>
										</li>
									</a>
									@endif						
								</ul>
							</div>
							@endif
							<div class="single-event-details">
								@include('modals.help')
								<a class="float-right" data-toggle="modal" data-target="#helpModal"><i class="fa fa-question-circle fa-2x" aria-hidden="true"></i></a>
								<h4>Required Documents<br><span class="small">(* denotes mandatory)</span></h4>
								<ul class="mt-10">
							@foreach(json_decode($page['event']->documents) AS $document)
									<li class="justify-content-between d-flex">
										<span>{{ $document->name }}
										@if($document->required==1)
										*
										@endif</span>
									</li>
							@endforeach
								</ul>
							</div>	
							<div class="single-event-details">
								<h4>Details</h4>
								<ul class="mt-10">
									<li class="justify-content-between d-flex">
										<span>Start date</span>
										<span>{{ \Carbon\Carbon::parse($page['event']->start)->format('D jS M Y') }}</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>End date</span>
										<span>{{ \Carbon\Carbon::parse($page['event']->finish)->format('D jS M Y') }}</span>
									</li>													
								</ul>
							</div>
							<div class="single-event-details">
								<h4>Venue</h4>
								<ul class="mt-10">
									<li class="justify-content-between d-flex">
										<span>Street</span>
										<span>{{ $page['event']->address_1 }}</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>Town</span>
										<span>{{ $page['event']->town }}</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>County</span>
										<span>{{ $page['event']->county }}</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>Postcode</span>
										<span>{{ $page['event']->postcode }}</span>
									</li>											
								</ul>
							</div>
							<div class="single-event-details">
								<h4>Organiser</h4>
								<ul class="mt-10">
									<li class="justify-content-between d-flex">
										<span>Company</span>
										<span>{{ $page['organiser']->organiser }}</span>
									</li>														
								</ul>
							</div>											
						</div>
					</div>
				</div>	
			</section>
			<!-- End event-details Area -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script>
	var chart1 = document.getElementById("completion");
	var myDoughnutChart = new Chart(chart1, {
	    type: 'doughnut',
	    data: {
    datasets: [{
        data: [{{ Document::completion($page['event']->id) }}, {{ 100-Document::completion($page['event']->id) }}],
            backgroundColor: [
                'rgba(0,255,0,0.5)',
                'rgba(184,184,184,0.5)'
            ],
    }],
        labels: [
        'Completed (%)',
        'Pending (%)'
    ]
}
	});
</script>
<script>
	var chart2 = document.getElementById("approved");
	var myDoughnutChart2 = new Chart(chart2, {
	    type: 'doughnut',
	    data: {
    datasets: [{
        data: [{{ Document::approved($page['event']->id) }}, {{ Document::submitted($page['event']->id)->submitted }}],
            backgroundColor: [
                'rgba(0,255,0,0.5)',
                'rgba(184,184,184,0.5)'
            ],
    }],
        labels: [
        'Approved',
        'Submitted'
    ]
}
	});
</script>
<script>
	var chart3 = document.getElementById("submitted");
	var myDoughnutChart2 = new Chart(chart3, {
	    type: 'doughnut',
	    data: {
    datasets: [{
        data: [{{ Document::submitted($page['event']->id)->submitted }}, {{ Document::submitted($page['event']->id)->total }}],
            backgroundColor: [
                'rgba(0,255,0,0.5)',
                'rgba(184,184,184,0.5)'
            ],
    }],
        labels: [
        'Submitted',
        'Total Expected'
    ]
}
	});
</script>
					