<div class="container py-5">
	<div class="row">
		<div class="col-md-12">
			<h2>Authentication Error</h2>
			<p>Sorry we have been unable to authenticate you, please click your login link in your emails or contact us if you are still experienceing problems.</p>
		</div>
	</div>
</div>