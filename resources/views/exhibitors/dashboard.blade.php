			<section class="top-category-widget-area pt-90 pb-90 ">
				<div class="container">
					<div class="row">
						@foreach($page['events'] AS $event)	
						<div class="col-lg-4">
							<a href="/events/{{ $event->id }}">
							<div class="single-cat-widget">
								<div class="content relative">
								      <div class="thumb">
								  		 <img class="content-image img-fluid d-block mx-auto" src="{{ Storage::url($event->image) }}" alt="{{ $event->event }}">
								  	  </div>
								</div>
							</div>
						      <div class="content-details">
						        <h4 class="content-title mx-auto text-uppercase">{{ $event->event }}</h4>
						        <span></span>								        
						        <p>{{ \Carbon\Carbon::parse($event->start)->format('D jS M Y') }} - {{ \Carbon\Carbon::parse($event->finish)->format('D jS M Y') }}</p>
						      </div>
							</a>
						</div>
						@endforeach							
					</div>
				</div>	
			</section>