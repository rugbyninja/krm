@extends('layout')

@section('content')
@if(isset($page['protected']))
	@include($page['protected'])
@else
	@if(isset($page['error']))
		@include('errors.'.$page['error'])
	@else
		<div class="container py-5">
			<div class="row">
				<div class="col-md-12">
					{!! $page['page']->content !!}
				</div>
			</div>
		</div>
	@endif
@endif
@endsection