<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventsTableDownloadName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('notes');
            $table->string('download_1_name')->nullable();
            $table->string('download_2_name')->nullable();
            $table->string('download_3_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventas', function (Blueprint $table) {
            $table->text('notes')->nullable();
            $table->dropColumn('download_1_name');
            $table->dropColumn('download_2_name');
            $table->dropColumn('download_3_name');
        });
    }
}
