<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event');
            $table->integer('organiser_id');
            $table->string('image');
            $table->text('description')->nullable();
            $table->string('download_1')->nullable();
            $table->string('download_2')->nullable();
            $table->string('download_3')->nullable();
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('town');
            $table->string('county');
            $table->string('postcode');
            $table->date('start');
            $table->date('finish');
            $table->text('documents');
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('twitter')->nullable();
            $table->string('website')->nullable();
            $table->text('notes')->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
