<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// All Admin / CMS Routes
Route::prefix('admin')->group(function(){
	// Loads the dashboard
	Route::get('/', 'AdminController@index');

	// All page routes
	Route::get('pages', 'PageController@admin');
	Route::get('pages/create', 'PageController@create');
	Route::get('pages/update/{id}', 'PageController@update');
	Route::post('pages/store', 'PageController@store');

	// All settings routes
	Route::get('settings', 'SettingsController@admin');
	Route::get('settings/update/{id}', 'SettingsController@update');
	Route::post('settings/store', 'SettingsController@store');

	// All organiser routes
	Route::get('organisers', 'OrganiserController@admin');
	Route::get('organisers/create', 'OrganiserController@create');
	Route::get('organisers/update/{id}', 'OrganiserController@update');
	Route::post('organisers/store', 'OrganiserController@store');

	// All event routes
	Route::get('events', 'EventController@admin');
	Route::get('events/create', 'EventController@create');
	Route::get('events/update/{id}', 'EventController@update');
	Route::post('events/store', 'EventController@store');
	Route::get('events/import', 'EventController@import');
	Route::get('events/exhibitors/{id}', 'EventController@exhibitors');
	Route::get('events/documents/{id}', 'EventController@documents');
	Route::get('events/approve/{exhibitor}', 'AttendeeController@manual_approve');
	Route::get('events/approve/{event}', 'AttendeeController@approve');
	Route::get('events/deadline/{event}', 'AttendeeController@deadline_all');
	Route::get('events/reminder/{event}', 'AttendeeController@reminder_all');
	Route::get('event/{event}/{exhibitor}/{document}', 'DocumentController@manual');

	// All exhibitor routes
	Route::get('exhibitors', 'ExhibitorController@admin');
	Route::get('exhibitors/create', 'ExhibitorController@create');
	Route::get('exhibitors/update/{id}', 'ExhibitorController@update');
	Route::post('exhibitors/store', 'ExhibitorController@store');
	Route::post('exhibitors/import', 'ExhibitorController@import');
	Route::get('exhibitors/documents/{event}/{id}', 'ExhibitorController@documents');
	Route::get('exhibitors/approve/{event}/{id}', 'AttendeeController@approve');
	
	// All Download Routes
	Route::get('download/{id}', 'DocumentController@download');

	// All Document Routes
	Route::post('document/approve/{id}', 'DocumentController@approve');
	Route::post('document/decline/{id}', 'DocumentController@decline');
	Route::post('document/message/{id}', 'DocumentController@message');
});

// All exhibitor routes
Route::prefix('exhibitor')->group(function(){

	// Shows the login form
	Route::get('login', 'PageController@login');
	Route::post('login', 'PageController@login');

	// Logs in the exhibitor
	Route::get('login/{id}/{email}', 'ExhibitorController@login');

	// Logs the exhibitor out
	Route::get('logout', 'ExhibitorController@logout');


	// Show the exhibitors dashboard
	Route::get('dashboard', 'ExhibitorController@index');

	// Stores a document upload
	Route::post('upload', 'DocumentController@store');

});

// All organiser routes
Route::prefix('organiser')->group(function(){

	// Shows the login form
	Route::get('login', 'OrganiserController@login');
	Route::post('login', 'OrganiserController@login');

	// Show the organisers dashboard
	Route::get('dashboard', 'OrganiserController@index');

	// Shows an individual events page for an organiser
	Route::get('event/{id}', 'OrganiserController@event');

});

// All event routes
Route::prefix('events')->group(function(){

	// Returns a view of a specific event
	Route::get('{id}', 'EventController@show');

});

Route::prefix('oops')->group(function (){

	// Returns error message page specified in slug
	Route::get('{slug}', 'ErrorController@view');
});

// CMS Pages
Route::get('/', 'PageController@index');
Route::get('/{slug}', 'PageController@view');